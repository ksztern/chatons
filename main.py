import os
from flask import Flask

app = Flask(__name__)


DEBUG = bool(os.environ.get('DEBUG', False))
PORT = int(os.environ.get('PORT', 5000))


@app.route("/")
def index():
    return "Bonjour chatons! mdr"


if __name__ == '__main__':
    app.run(debug=DEBUG, host='0.0.0.0', port=PORT)
