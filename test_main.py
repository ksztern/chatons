import main
import unittest


class FlaskTest(unittest.TestCase):
    def setUp(self):
        self.app = main.app.test_client()

    def tearDown(self):
        pass

    def test_index(self):
        ret = self.app.get("/")
        assert "chatons" in ret.data


if __name__ == '__main__':
    unittest.main()
